package com.broker.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.broker.bo.SearchConditionBo;
import com.broker.crm.vo.QueryResultVo;
import com.broker.dao.rdb.impl.SysLinkDAOImpl;
import com.broker.model.PageInfo;
import com.broker.model.rdb.SysLink;
import com.broker.repository.rdb.SysLinkRepository;
import com.broker.vo.SyslinkVo;

@Service
@Transactional("jpaTransactionManager")
public class SyslinkService {
	
	//this is 6
	
	public static final Logger logger = Logger.getLogger(SyslinkService.class);
	
	@Autowired
	private SysLinkDAOImpl syslinkDAOImpl;
	
	
	public List<SyslinkVo> querySyslink(){
		
		
		List<SyslinkVo> syslinkRecordList = new ArrayList<SyslinkVo>();
		
		String count = syslinkDAOImpl.countSysLink(); // reason : select 's result is string
		int countInt = Integer.parseInt(count);  // string to int
		
		for(int i=1;i<=countInt;i++){
			SyslinkVo syslinkVo = new SyslinkVo();	
			
			/*
			Random ran = new Random();
			syslinkVo.setSort(i);
			syslinkVo.setItem(String.valueOf(ran.nextInt(50)));
			syslinkVo.setLink(String.valueOf(ran.nextInt(1000)));
			syslinkVo.setOpenmethod(String.valueOf(ran.nextInt(40)));
			syslinkVo.setTooltip(String.valueOf(ran.nextInt(60))); 
			/*假資料假資料假資料假資料假資料*/
			*/
			
			syslinkVo.setItem(syslinkDAOImpl.rItem(String.valueOf(i)));
			syslinkVo.setSort(syslinkDAOImpl.rSort(String.valueOf(i)));
			syslinkVo.setLink(syslinkDAOImpl.rLink(String.valueOf(i)));
			syslinkVo.setOpenmethod(syslinkDAOImpl.rOpenmethod(String.valueOf(i)));
			syslinkVo.setTooltip(syslinkDAOImpl.rTooltip(String.valueOf(i)));
			
			syslinkRecordList.add(syslinkVo);	
			
			
		}

		return syslinkRecordList;
		
	}
	 

}
